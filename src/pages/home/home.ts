import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import  leaflet from 'leaflet';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  redMarker = leaflet.AwesomeMarkers.icon({
    icon: 'coffee',
    markerColor: 'red'
  });

  constructor(public navCtrl: NavController) {
    
  }
  ionViewDidEnter() {
    this.loadmap();
  }
  loadmap() {
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18
    }).addTo(this.map);
    this.map.locate({
      setView: true,
      maxZoom: 10
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude],{icon: this.redMarker }).on('click', () => {
        alert('marcador');
      })
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
    })
      .on('click', (e) => {

        let mapL = e.latlng;
        //leaflet.marker([mapL.lat,mapL.lng]).addTo(this.map);
        console.log(mapL.lat + " , " + mapL.lng);
        //let latlngs = [e.longitude,e.latitude];
        alert(mapL.lat + " , " + mapL.lng);
      })
      .on('locationerror', (err) => {
        alert(err.message);
      })

  }
}